from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.views.decorators.cache import cache_page

from base import views as base_views
from blog.sitemaps import ArticleSitemap

sitemaps = {
    'posts': ArticleSitemap
}

urlpatterns = [
    url(r'^sitemap\.xml', sitemap, {'sitemaps': sitemaps}, name='sitemap'),
    url(r'^api/team/', include('accounts.urls', namespace='accounts')),
    # Для создания разных приложений разные версии АПИ url(r'^api/v1/article/', include('blog.urls', namespace='blog')),
    url(r'^api/article/', include('blog.urls', namespace='blog')),
    url(r'^api/events/', include('event.urls', namespace='event')),
    url(r'^api/getdata/', include('base.urls', namespace='base')),
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^admin/', admin.site.urls),
    url(r'^mdeditor/', include('mdeditor.urls'))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [url(r'', cache_page(settings.PAGE_CACHE_SECONDS)(base_views.IndexView.as_view()), name='index')]