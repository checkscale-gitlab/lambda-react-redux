import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import * as actionCreators from "../../actions/team";
import PreloaderIcon, {ICON_TYPE} from 'react-preloader-icon';
import {ImageBackgound} from '../../components/ImageBackgound/index'
import {Title} from '../../components/Title/index'
import {Panel} from '../../components/Panel/index'
import {Teammate} from '../../components/Teammate/index'
import logo from './about_bg.png';

class AboutView extends React.Component {

    static propTypes = {
        results: PropTypes.array.isRequired,
        count: PropTypes.number.isRequired,
        next: PropTypes.string,
        previous: PropTypes.string,
        actionsTeam: PropTypes.shape({
            fetchTeam: PropTypes.func.isRequired,
            fetchTeamEvents: PropTypes.func.isRequired,
        }).isRequired
    };

    static defaultProps = {
        results: [],
        count: 0,
        next: '',
        previous: ''
    };

    componentDidMount() {
        this.props.actionsTeam.fetchTeam();
        this.props.actionsTeam.fetchTeamEvents();
        console.log(this.props)

    }

    render() {
        console.log(this.props);

        return (
            <React.Fragment>
                <React.Fragment>
                    <ImageBackgound image={logo} height={450}>
                        <Title text={"О нас"} color={'#fff'} line_color={'#f74236'}/>
                    </ImageBackgound>
                    <Panel>
                        Мы — студенческий практикум программистов при Московском Авиационном Институте.

                        «Лямбда» — это:
                        🔸лекции и обмен опытом;
                        🔸хакатоны и олимпиады;
                        🔸площадка для выступлений экспертов из IT компаний.

                        Если Вы:
                        💻 хотите создавать сайты, приложения или игры в команде студентов;
                        📚 нагружены теорией программирования и хотите применить свои навыки;
                        😎 программист и хотите поделиться опытом со студентами;
                        🚩 только в начале пути и хотите научиться практическому программированию и разработке,
                        то обязательно приходите на наши собрания. Скучно не будет точно!
                    </Panel>
                </React.Fragment>
                {this.props.team.team.results
                    ?
                    <React.Fragment>
                        <Title second={true} second_text={'НАША КОМАНДА'}
                               text={"Лучшие нашей <b>Команды</b>"}
                               color={'#666'} line_color={'#f74236'}
                        />
                        <div className={'teammate-grid'}>
                            {this.props.team.team.results && this.props.team.team.results.map(item => {
                                return <Teammate
                                    image={item.images.png.thumbnail_big}
                                    first_name={item.first_name}
                                    last_name={item.last_name}
                                    position={item.position}
                                    social={item.social_networks}
                                    key={item.images.png.thumbnail_big}
                                />
                            })}
                        </div>
                        <Title second={true} second_text={'МЫ УЧАСТВУЕМ'}
                               text={"Мы принимали <b>участие</b> в"}
                               color={'#666'} line_color={'#f74236'}
                        />
                        <div className={'panel-grid'}>
                            {this.props.team.event.results && this.props.team.event.results.map(item => {
                                return <div className={'panel-item'} key={item.url}>
                                    <a href={item.url} target={'_blank'}>
                                        <img src={item.images.png.thumbnail} alt={item.name}/>
                                    </a>
                                </div>
                            })}
                        </div>
                    </React.Fragment>
                    :
                    <div className='preloader'>
                        <PreloaderIcon
                            type={ICON_TYPE.PUFF}
                            size={60}
                            strokeWidth={8}
                            strokeColor="#4d4d4d"
                            duration={800}
                        />
                    </div>
                }
            </React.Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        team: state.team,
        event: state.event,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actionsTeam: bindActionCreators(actionCreators, dispatch),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AboutView);
export {AboutView as AboutViewNotConnected};
