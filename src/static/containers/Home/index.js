import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import * as actionCreators from "../../actions/article";
import PreloaderIcon, {ICON_TYPE} from 'react-preloader-icon';
import {Feed} from '../../components/Feed/index';
import {Helmet} from "react-helmet";

class HomeView extends React.Component {

    static propTypes = {
        results: PropTypes.array.isRequired,
        count: PropTypes.number.isRequired,
        next: PropTypes.string,
        previous: PropTypes.string,
        actionsPosts: PropTypes.shape({
            fetchArticle: PropTypes.func.isRequired,
            fetchArticlesList: PropTypes.func.isRequired,
        }).isRequired
    };

    static defaultProps = {
        results: [],
        count: 0,
        next: '',
        previous: ''
    };

    componentDidMount() {
        this.props.actionsPosts.fetchArticlesList();
    }

    render() {
        return (<div>
                <Helmet>
                    <title>Lambda</title>
                    <link rel="canonical" href="https://lambda-it.ru"/>
                    /* Vk & Facebook */
                    <meta property="og:locale" content="ru_RU"/>
                    <meta property="og:type" content="article"/>
                    <meta property="og:title" content="Lambda"/>
                    <meta property="og:description" content="Мы — студенческий практикум программистов при Московском Авиационном Институте.«Лямбда» — это:
🔸лекции и обмен опытом;
🔸хакатоны и олимпиады;
🔸площадка для выступлений экспертов из IT компаний.
Если Вы:хотите создавать сайты, приложения или игры в команде студентов;
нагружены теорией программирования и хотите применить свои навыки;
программист и хотите поделиться опытом со студентами; только в начале пути и хотите научиться практическому программированию и разработке,
то обязательно приходите на наши собрания. Скучно не будет точно!"/>
                    <meta property="og:url" content={window.location.href}/>
                    <meta property="og:site_name" content="Lambda"/>
                    /* Twitter */
                    <meta name="twitter:card" content="summary"/>
                    <meta name="twitter:site" content="Lambda"/>
                    <meta name="twitter:title" content="Lambda"/>
                    <meta name="twitter:description" content="Мы — студенческий практикум программистов при Московском Авиационном Институте.«Лямбда» — это:
🔸лекции и обмен опытом;
🔸хакатоны и олимпиады;
🔸площадка для выступлений экспертов из IT компаний.
Если Вы:хотите создавать сайты, приложения или игры в команде студентов;
нагружены теорией программирования и хотите применить свои навыки;
программист и хотите поделиться опытом со студентами; только в начале пути и хотите научиться практическому программированию и разработке,
то обязательно приходите на наши собрания. Скучно не будет точно!"/>
                    /* Google and other */
                    <meta itemprop="name" content="Lambda"/>
                    <meta itemprop="description" content="Мы — студенческий практикум программистов при Московском Авиационном Институте.«Лямбда» — это:
🔸лекции и обмен опытом;
🔸хакатоны и олимпиады;
🔸площадка для выступлений экспертов из IT компаний.
Если Вы:хотите создавать сайты, приложения или игры в команде студентов;
нагружены теорией программирования и хотите применить свои навыки;
программист и хотите поделиться опытом со студентами; только в начале пути и хотите научиться практическому программированию и разработке,
то обязательно приходите на наши собрания. Скучно не будет точно!"/>
                </Helmet>
                {this.props.posts.postsList.results
                    ?
                    <Feed list={this.props.posts.postsList.results}/>
                    :
                    <div className='preloader'>
                        <PreloaderIcon
                            type={ICON_TYPE.PUFF}
                            size={60}
                            strokeWidth={8}
                            strokeColor="#4d4d4d"
                            duration={800}
                        />
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state.posts,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actionsPosts: bindActionCreators(actionCreators, dispatch),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeView);
export {HomeView as HomeViewNotConnected};
