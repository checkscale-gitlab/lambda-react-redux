import {Header as Base} from './Header'
import {headerTransparent} from './_transparent/Header_transparent'
import {compose} from "@bem-react/core";


export const Header = compose(
    headerTransparent,
)(Base);
