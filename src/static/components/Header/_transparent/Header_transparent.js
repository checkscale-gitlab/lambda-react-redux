import {cn} from '@bem-react/classname';
import {withBemMod} from '@bem-react/core'
import {Icon} from "../../Icon";
import {ICONS} from "../../Icon/icon_constants";
import {Sidebar} from "../../Sidebar";
import React, {useState} from 'react';
import './transparent.sass'

const header = cn('header');

const Header_transparent = (w, props) => {
    const [sidebar, setSidebar] = useState(false);
    const toogleSidebar = event => setSidebar(!sidebar);
    let head_menu = [
        {
            text: 'Публикации',
            to: '/'
        }, {
            text: 'О нас',
            to: '/about'
        },
    ];
    return (

        <div className={header({transparent: true})}>
            <div className={header("menu")} onClick={toogleSidebar}>
                <Icon size={30} icon={ICONS.MENU} color={"#d8d8d8"}/>
            </div>
            <div className={header("logo")}>
                <Icon width={100} height={22} icon={ICONS.LAMBDA} color={"#4D4D4D"}/>
            </div>
            <ul className={header("socials")}>
                <li className={header("item")}>
                    <a className={header("link")} href="https://www.instagram.com/lambda_mai/">
                        <Icon size={30} icon={ICONS.INSTAGRAM} color={"#ccc"} hoverColor={"#000"}/>
                    </a>
                </li>
            </ul>
            <Sidebar list={head_menu} toogle={sidebar}/>
        </div>
    );
};

export const headerTransparent = withBemMod(
    'asome',
    {transparent: true},
    Header_transparent
);