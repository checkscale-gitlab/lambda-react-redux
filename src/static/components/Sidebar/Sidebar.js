import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import {Link} from "react-router-dom";
import './sidebar.sass'

const feed = cn('sidebar');

export class Sidebar extends Component {


    state = {
        open: false
    };
    // TODO: Херово работающий toogle sidebar
    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.toogle > this.props.toogle
        });
    };

    _toogleSidebar() {
        this.setState({open: !this.props.toogle});
    }

    render(w, props) {
        return (
            <div className={feed({open: this.state.open})}>
                <ul className={feed('list')}>
                    {
                        this.props.list && (this.props.list.map(item => {
                            return (
                                <li key={item.text} className={feed('text')}>
                                    <Link onClick={this._toogleSidebar.bind(this)} to={item.to}
                                          className={feed("link")}>{item.text}</Link>
                                </li>
                            )
                        }))
                    }

                </ul>
            </div>
        );
    };
}