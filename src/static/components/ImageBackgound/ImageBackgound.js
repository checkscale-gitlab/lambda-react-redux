import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import './imagebackground.sass'

const image = cn('imagebackgound');

export class ImageBackgound extends Component {

    render() {
        return (
            <div className={image()}
                 style={{backgroundImage: `url(${this.props.image})`, height: this.props.height}}>
                <div className={image('wrapper')}>
                    <div className={image('content')}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

