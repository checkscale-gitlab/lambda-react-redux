import {Title as Base} from './Title'
import {secondTitle} from './_second/Title_second'
import {compose} from "@bem-react/core";


export const Title = compose(
    secondTitle,
)(Base);
