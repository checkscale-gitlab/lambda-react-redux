import {cn} from '@bem-react/classname';
import {withBemMod} from '@bem-react/core'
import React from 'react';
import './second_title.sass'

const cls = cn('title');

const Title_second = (w, props) => {

    return (

        <div className={cls()}>
            <div className={cls('second')}>
                {props.second_text}
            </div>
            <h3 className={cls('text')} style={{color: props.color}}
                dangerouslySetInnerHTML={{__html: props.text}}>
            </h3>
            <div className={cls('line')} style={{borderColor: props.line_color}}></div>
        </div>
    );
};

export const secondTitle = withBemMod(
    'asome',
    {second: true},
    Title_second
);