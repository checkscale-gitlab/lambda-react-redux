import React from 'react';
import {cn} from '@bem-react/classname';
import {Icon} from "../Icon";
import {ICONS} from "../Icon/icon_constants";
import './articleheader.sass'

const article_header = cn('articleheader');
export const ArticleHeader = (w, props) => {
    // detect browser
    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    let isFirefox = typeof InstallTrigger !== 'undefined';
    let img_path = '';
    // all browser exect firefox chrome, not work with webp
    if (!isChrome && !isFirefox) {
        img_path = w.images.png.compressed;
    } else {
        img_path = w.images.webp.compressed;
    }
    return (
        <div className={article_header()}
             style={{backgroundImage: `url(${img_path})`, backgroundPosition: 'center',}}>
            <div className={article_header("content")}>
                <div className={article_header("main")}>
                <span className={article_header("tag")} style={{backgroundColor: `${w.category.color}`}}>
                    {w.category.name}
                </span>
                    <span className={article_header("date")}>
                        {w.date_create}
               </span>
                    <h1 className={article_header("title")}>
                        {w.title}
                    </h1>
                    <span className={article_header("line")} style={{borderColor: `${w.category.color}`}}></span>
                </div>
                <div className={article_header("tools")}>
                </div>
                <div className={article_header("info")}>

                    <span className={article_header("item")}>
                        <Icon size={30} icon={ICONS.PROFILE} color={"#ccc"}/>
                        <span className={article_header("value")}>{w.author.name}</span>
                    </span>
                    <span className={article_header("item")}>
                        <Icon size={30} icon={ICONS.SCHEDULE} color={"#ccc"}/>
                        <span className={article_header("value")}>{w.time_read} чтения</span>
                    </span>
                </div>
            </div>
        </div>
    );
};

