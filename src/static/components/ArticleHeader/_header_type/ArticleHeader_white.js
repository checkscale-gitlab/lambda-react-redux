import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from '@bem-react/core'
import {Icon} from "../../Icon";
import {ICONS} from "../../Icon/icon_constants";
import './articleheader_white.sass'

const article_header = cn('articleheader');

const ArticleHeader_white = (w, props) => {
    return (
        <div className={article_header({white: true})}
             style={{backgroundColor: "#fff"}}>
            <div className={article_header("content")}>
                <div className={article_header("tools")}>
                </div>

                <div className={article_header("main")}>
                    <span className={article_header("tag")} style={{backgroundColor: `${props.category.color}`}}>
                        {props.category.name}
                    </span>
                    <span className={article_header("date")}>
                            {props.date_create}
                    </span>
                    <h1 className={article_header("title")}>
                        {props.title}
                    </h1>
                    <span className={article_header("line")} style={{borderColor: `${props.category.color}`}}></span>
                </div>
                <div className={article_header("tools")}>
                </div>
                <div className={article_header("info")}>
                    <span className={article_header("item")}>
                        <Icon size={30} icon={ICONS.PROFILE} color={"#ccc"}/>
                        <span className={article_header("value")}>{props.author.name}</span>
                    </span>
                    <span className={article_header("item")}>
                        <Icon size={30} icon={ICONS.SCHEDULE} color={"#ccc"}/>
                        <span className={article_header("value")}>{props.time_read} чтения</span>
                    </span>
                </div>
            </div>
        </div>
    );
};

export const headerWhite = withBemMod(
    'asome',
    {header_type: 'white'},
    ArticleHeader_white
)