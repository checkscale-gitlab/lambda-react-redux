import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import './teammate.sass'
import {Icon} from "../Icon";
import {ICONS} from "../Icon/icon_constants";

const cls = cn('teammate');

export class Teammate extends Component {
    render() {
        return (
            <div className={cls()}>
                <div className={cls('img')}
                     style={{backgroundImage: `url(${this.props.image})`}}>
                    <div className={cls('description')}>
                        <div className={cls('name')}>
                            {this.props.first_name} {this.props.last_name}
                        </div>
                        <div className={cls('position')}>
                            {this.props.position}
                        </div>
                    </div>
                    <div className={cls('social')}>
                        {this.props.social && this.props.social.map(item => {
                            return <a key={item.url} className={cls('icon')} target={'_blank'} href={item.url}>
                                {(() => {
                                    switch (item.name) {
                                        case "github":
                                            return <Icon size={30} icon={ICONS.INSTAGRAM} color={"#fff"}
                                                         hoverColor={"#000"}/>;
                                        case "vk":
                                            return <Icon size={30} icon={ICONS.VK} color={"#fff"} hoverColor={"#000"}/>;
                                        case "twitter":
                                            return <Icon size={30} icon={ICONS.TWITTER} color={"#fff"}
                                                         hoverColor={"#000"}/>;
                                        default:
                                            return "#FFFFFF";
                                    }
                                })()}

                            </a>
                        })}

                    </div>
                </div>
            </div>
        )
    }
}

