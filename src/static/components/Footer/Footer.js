import React from 'react';
import {cn} from '@bem-react/classname';
import {Icon} from "../Icon";
import {ICONS} from "../Icon/icon_constants";
import {Link} from "react-router-dom";
import './footer.sass'

const footer = cn('footer');

export const Footer = (w, props) => {

    return (
        <div className={footer()}>

            <div className={footer('list')}>
                <div className={footer('wrapper')}>
                    <div className={footer('copyright')}>
                        <Icon width={102} height={24} icon={ICONS.LAMBDA} color={"#666666"}/>
                        <span>Privacy Policy</span>
                        <span>© 2018 Lambda Inc</span>
                    </div>
                    <div className={footer("navigation")}>
                        <span>Навигация</span>
                        <ul>
                            {
                                w.menu && w.menu.map((item, index) => {
                                    return <li key={item.text}>
                                        <Link to={item.to}>{item.text}</Link>
                                    </li>
                                })
                            }
                        </ul>
                    </div>
                    <div className={footer("contacts")}>
                        <span>Контакты</span>
                        <div className={footer("phone")}>
                            <a href={`tel:${w.contacts.phone}`}>{w.contacts.phone}</a>
                            <a href={`mailto:${w.contacts.email}`}>{w.contacts.email}</a>
                        </div>
                        <hr/>
                        <div className={footer("socials")}>
                            {
                                w.socials.map(item => {
                                    return <a key={item.to}  target={"blank"} href={item.to}><Icon width={30} height={30}
                                                                                  icon={item.icon}
                                                                                  color={"#d8d8d8"}
                                                                                  hoverColor={"#007bff"}/></a>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

