import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from "@bem-react/core";
import {Image} from "../../Image";
import './articlecontent_gallery.sass';

const article_content = cn('articlecontent');
const article_content_main = cn('articlecontent-main');

export const ArticleContent_gallery = (w, props) => {

    return (
        <div className={article_content_main()}>
            <div className={article_content({gallery: true})}>
                {props.content && props.content.map((el, index) =>
                    <Image key={index} size={el.size} image={el.url} caption={el.caption}
                           thumbnail={el.url}/>
                )}
            </div>
        </div>

    )
};


export const contentGallery = withBemMod(
    'asome',
    {block: 'gallery'},
    ArticleContent_gallery
);