import React from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from "@bem-react/core";
import Highlight from 'react-highlight';
import './articlecontent_code.sass';
import './darcula.css';

const article_content = cn('articlecontent');
const article_content_main = cn('articlecontent-main');

export const ArticleContent_code = (w, props) => {

    return (
        <div className={article_content_main()}>
            <div className={article_content({code: true})}>
                <Highlight className={props.content.lang}>
                    {props.content.code}
                </Highlight>
            </div>
        </div>
    )
};


export const contentCode = withBemMod(
    'asome',
    {block: 'code'},
    ArticleContent_code
);