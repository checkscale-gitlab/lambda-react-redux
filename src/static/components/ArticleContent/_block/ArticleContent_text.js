import React from 'react';
import {cn} from '@bem-react/classname';
import './articlecontent_text.sass'
import {withBemMod} from "@bem-react/core";

const article_content = cn('articlecontent');
const article_content_main = cn('articlecontent-main');

export const ArticleContent_text = (w, props) => {
    /*
    *  Функция для подргузки изображений
    *  Пример взят от сюда https://codepen.io/codebeast/pen/VQgyrN
    * */
    /* setTimeout(function () {
        const images = document.querySelectorAll('img');
        console.log(images);
        const options = {
            // If the image gets within 50px in the Y axis, start the download.
            root: null, // Page as root
            rootMargin: '0px',
            threshold: 0.1
        };

        const fetchImage = (url) => {
            console.log(url)
            return new Promise((resolve, reject) => {
                const image = new Image();
                image.src = url;
                image.onload = resolve;
                image.onerror = reject;
            });
        }

        const loadImage = (image) => {
            const src = image.src;
            fetchImage(src).then(() => {
                console.log(src)
                image.src = src;
            })
        }

        const handleIntersection = (entries, observer) => {
            entries.forEach(entry => {
                if (entry.intersectionRatio > 0) {
                    console.log(entry.intersectionRatio);
                    loadImage(entry.target)
                }
            })
        }

        // The observer for the images on the page
        const observer = new IntersectionObserver(handleIntersection, options);

        images.forEach(img => {
            observer.observe(img);
        })
    }, 100); */

    return (
        <div className={article_content_main()}>
            <div className={article_content({text: true})} dangerouslySetInnerHTML={{__html: props.content}}>
            </div>
        </div>
    );
}


export const contentText = withBemMod(
    'asome',
    {block: 'text'},
    ArticleContent_text
);