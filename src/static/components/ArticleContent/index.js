import {compose} from '@bem-react/core'

import {ArticleContent as Base} from './ArticleContent'
import {contentText} from './_block/ArticleContent_text'
import {contentCarousel} from './_block/ArticleContent_carousel'
import {contentGallery} from './_block/ArticleContent_gallery'
import {contentCode} from './_block/ArticleContent_code'
import {contentQuote} from './_block/ArticleContent_quote'


export const ArticleContent = compose(
    contentText,
    contentGallery,
    contentCode,
    contentQuote,
    contentCarousel)(Base);
