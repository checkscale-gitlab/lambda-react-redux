import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import {Button} from "../Button";
import {Icon} from "../Icon";
import {ICONS} from "../Icon/icon_constants";
import ReactDOM from 'react-dom'
import {Intense} from './Intense'
import './image.sass'

const image = cn('image');

export class Image extends Component {

    componentDidMount() {
        Intense(ReactDOM.findDOMNode(this).children[0]);
    }

    render() {
        return (
            <div className={image({size: this.props.size})}>
                <div data-image={this.props.image} data-title={this.props.caption} className={image("view")}>
                    <Button type={"circle"}
                            icon={<Icon size={30} icon={ICONS.SEARCH} color={"#fff"}/>}/>
                </div>
                <img src={this.props.thumbnail} alt={this.props.caption}/>
                <span className={image("caption")}>{this.props.caption}</span>
            </div>
        )
    }
}

