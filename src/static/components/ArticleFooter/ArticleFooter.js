import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import {Icon} from "../Icon";
import {ICONS} from "../Icon/icon_constants";
import {Button} from "../Button/index";
import './articlefooter.sass'
import {FacebookShareButton, FacebookShareCount, TwitterShareButton, VKShareButton, VKShareCount} from 'react-share';

const article_footer = cn('articlefooter');

export class ArticleFooter extends Component {


    _toTop() {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

    render(w, props) {
        const divStyle = {
            justifyContent: 'center',
        };
        const {author, tags, title, image, short_description} = this.props;
        return (
            <div className={article_footer()}>
                <div className={article_footer("top")}>
                    <div className={article_footer("item")}>
                        <div className={article_footer("author")}>

                            <picture>
                                <source srcSet={author.images.png.thumbnail}/>
                                <img src={author.images.webp.thumbnail} alt={author.name}/>
                            </picture>

                            <div className={article_footer("info")}>
                                <div className={article_footer("name")}>{author.name}</div>
                                <div className={article_footer("position")}>{author.position}</div>
                            </div>
                        </div>
                    </div>
                    <div className={article_footer("item")} style={divStyle}>
                    </div>
                    <div className={article_footer("item")}>
                        <Button icon={<Icon size={30} icon={ICONS.ARROW_UP} color={"#d8d8d8"}/>} text='Наверх'
                                onClick={this._toTop.bind(this)} type='ghost' icon_align='right'/>
                    </div>
                </div>
                <div className={article_footer("bottom")}>
                    <div className={article_footer("tags")}>
                        {
                            tags.map((tag, index) => {
                                return <Button key={index} text={tag} type='label'/>
                            })
                        }
                    </div>
                    <div className={article_footer("share")}>

                        <span className={article_footer("social")}>
                            <VKShareButton url={window.location.href} description={short_description} title={title}
                                           image={image}>
                                <Icon size={25} icon={ICONS.VK} color={"#d8d8d8"}/>
                            </VKShareButton>
                        </span>
                        <span className={article_footer("count")}>
                            <VKShareCount url={window.location.href}>
                                {shareCount => (
                                    <span>{shareCount}</span>
                                )}
                            </VKShareCount>
                        </span>
                        <span className={article_footer("social")}>
                            <FacebookShareButton url={window.location.href} description={short_description}
                                                 title={title}
                                                 image={image}>
                                <Icon size={30} icon={ICONS.FACEBOOK} color={"#d8d8d8"}/>
                            </FacebookShareButton>
                        </span>
                        <span className={article_footer("count")}>
                            <FacebookShareCount url={window.location.href}>
                                {shareCount => (
                                    <span>{shareCount}</span>
                                )}
                            </FacebookShareCount>
                        </span>

                        <span className={article_footer("social")}>
                            <TwitterShareButton url={window.location.href} description={short_description} title={title}
                                                image={image}>
                                <Icon size={30} icon={ICONS.TWITTER} color={"#d8d8d8"}/>
                            </TwitterShareButton>
                        </span>

                    </div>

                </div>
            </div>
        );
    }
}