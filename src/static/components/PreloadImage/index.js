import React, {Component} from 'react';
import './preload.css'

var classNames = require('classnames');

export default class PreloadImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            src: null
        };
    }

    componentDidMount() {
        if (this.props.lazy && 'IntersectionObserver' in window) {
            this.setObserver();
        } else {
            this.setPreloader();
        }
    }

    setObserver() {
        this.observer = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    this.setPreloader();
                    this.observer.disconnect();
                }
            });
        });
        this.observer.observe(this.el);

    }

    setPreloader() {
        this.preloader = new Image();
        this.preloader.onload = () => this.setState({
            loaded: true,
            src: this.props.src.content
        });
        this.preloader.src = this.props.src.content;
    }

    componentWillUnmount() {
        if (this.observer) this.observer.disconnect();

    }


    render() {
        const {props: {className, src}} = this,
            classImage = classNames(className, 'preload-image', {'_loaded': this.state.loaded});

        return (
            <div className={classImage} ref={(el) => this.el = el}>
                <picture className="preload">
                    <source srcSet={src.preload_sec}/>
                    <img src={src.preload} alt="preload" className="preload"/>
                </picture>
                <picture className="content">
                    <source srcSet={src.content_sec}/>
                    <img src={this.state.src} alt="content" className="content"/>
                </picture>
            </div>
        );
    }
}