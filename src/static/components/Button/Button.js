import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import './button.sass'

const button = cn('button');

export class Button extends Component {


    handleClick(e) {
        this.props.onClick && this.props.onClick();
    }

    render() {
        const content = [];
        this.props.icon && content.push(React.cloneElement(this.props.icon, {key: 'icon'}));
        this.props.text && content.push(<span className={button("text")} key="button">{this.props.text}</span>);
        return (
            <button
                onClick={this.handleClick.bind(this)}
                className={button({
                    type: this.props.type,
                    icon_align: this.props.icon_align,
                    color: this.props.color
                })}>
                {content}
            </button>
        );
    }
}

