import React from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from '@bem-react/core'
import {Link} from "react-router-dom";
import './card_small.sass'
import PreloadImage from "../../PreloadImage";

const card = cn('card');

const Card_small = (w, props) => {
    return (
        <div className={card({small: true})}>
            <div className={card('head')}>
                <span className={card('tag')}
                      style={{'backgroundColor': props.category.color}}>{props.category.name}</span>
                <PreloadImage
                    className={card('thumbnail')}
                    src={{
                        preload: props.images.webp.preload,
                        content: props.images.webp.thumbnail,
                        preload_sec: props.images.png.preload,
                        content_sec: props.images.png.thumbnail,
                    }}
                    lazy
                    key={new Date()} // for reset

                />
            </div>
            <div className={card('body')}>
                <Link to={`/post/${props.slug}`} className={card('title')}>
                    <span>{props.title}</span>
                </Link>
            </div>
            <div className={card('footer')}>
                <div className={card('date')}>
                    <span>{props.date}</span>
                </div>
                <div className={card('views')}>
                    <span>{props.time_read} чтения</span>
                </div>
            </div>
        </div>
    );
};

export const cardSmall = withBemMod(
    card(),
    {size: 's'},
    Card_small
);
