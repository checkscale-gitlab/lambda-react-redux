/**
 * Created by mr9bit on 09.02.18.
 */
import {RECEIVE_TEAM_LIST, REQUEST_TEAM_LIST, REQUEST_TEAMEVENTS_LIST, RECEIVE_TEAMEVENTS_LIST} from "../constants";

const initialState = {
    team: {
        results: [],
        count: 0,
        next: '',
        previous: '',
        isFetching: false,
    },
    event: {
        results: [],
        count: 0,
        next: '',
        previous: '',
        isFetching: false,
    }
};
export default function teamsReducer(state = initialState, action) {
    switch (action.type) {
        case REQUEST_TEAMEVENTS_LIST:
            return Object.assign({}, state, {
                event: {
                    isFetching: true,
                    results: state.results,
                }
            });

        case REQUEST_TEAM_LIST:
            return Object.assign({}, state, {
                team: {
                    isFetching: true,
                    results: state.results,
                }
            });
        case RECEIVE_TEAMEVENTS_LIST: {
            /*
             * Сравнение двух списков статей если есть отличие, то соеденить их.
             */
            return Object.assign({}, state, {
                event: {
                    results: action.results,
                    count: action.count,
                    next: action.next,
                    previous: action.previous,
                    isFetching: false,
                }
            });
        }
        case RECEIVE_TEAM_LIST: {
            /*
             * Сравнение двух списков статей если есть отличие, то соеденить их.
             */
            return Object.assign({}, state, {
                team: {
                    results: action.results,
                    count: action.count,
                    next: action.next,
                    previous: action.previous,
                    isFetching: false,
                }
            });
        }


        default:
            return state
    }
}