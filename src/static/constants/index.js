/*
 * Константы для статьи
*/
export const REQUEST_ARTICLE = 'REQUEST_ARTICLE';
export const RECEIVE_ARTICLE = 'RECEIVE_ARTICLE';
/*
 * Константы для списка статей
*/
export const REQUEST_ARTICLE_LIST = 'REQUEST_ARTICLE_LIST';
export const RECEIVE_ARTICLE_LIST = 'RECEIVE_ARTICLE_LIST';
/*
 * Константы для списка участников
*/
export const REQUEST_TEAM_LIST = 'REQUEST_TEAM_LIST';
export const RECEIVE_TEAM_LIST = 'RECEIVE_TEAM_LIST';
/*
 * Константы для списка мероприятий где мы участвовали
*/
export const REQUEST_TEAMEVENTS_LIST = 'REQUEST_TEAMEVENTS_LIST';
export const RECEIVE_TEAMEVENTS_LIST = 'RECEIVE_TEAMEVENTS_LIST';