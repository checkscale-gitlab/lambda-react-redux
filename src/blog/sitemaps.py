from django.contrib.sitemaps import Sitemap

from blog.models import Article


class ArticleSitemap(Sitemap):
    changefreq = "always"
    priority = 0.9

    def items(self):
        return Article.objects.all().filter(status='p')

    def lastmod(self, obj):
        return obj.creation_date
