from bs4 import BeautifulSoup
import re
from martor.utils import markdownify


def pars_article(description):
    """
        Получить описание.
        Вот тут надо написать парсер.
        #TODO 2. Добавить кэширование статьи.
        #TODO 3. Если дата статьи изменилась, то создать новый кэш.
    :return: Массив элементов согласно issue #19
    """
    json_serializer = []
    soup = BeautifulSoup(description, 'html.parser')
    for item in soup.find_all():
        if item.name == 'text':
            json_serializer.append({
                'block': 'text',
                'content': markdownify(item.text[:len(item.text) - 1])
            })
        elif item.name == 'qoute':
            json_serializer.append({
                'block': 'quote',
                'content': {
                    'text': re.findall("\[(.*)\]", item.text)[0],
                    'author': re.findall("{(.*)}", item.text)[0]
                }
            })
        elif item.name == 'code':
            code = re.search('```', item.text)
            json_serializer.append({
                'block': 'code',
                'content': {
                    'lang': re.findall("{(.*)}", item.text)[0],
                    'code': item.text[code.end() + 2:len(item.text) - 6]
                }
            })
        elif item.name == 'carousel':
            images = []
            all_caption = re.findall("\[(.*)\]", item.text)
            all_url = re.findall("\((.*)\)", item.text)
            for i in range(len(all_url)):
                images.append({
                    'caption': all_caption[i],
                    'url': all_url[i],
                })
            json_serializer.append({
                'block': 'carousel',
                'content': images
            })
        elif item.name == 'gallery':
            images = []
            all_caption = re.findall("\[(.*)\]", item.text)
            all_url = re.findall("\((.*)\)", item.text)
            all_size = re.findall("{(.*)}", item.text)
            for i in range(len(all_url)):
                images.append({
                    'caption': all_caption[i],
                    'url': all_url[i],
                    'size': all_size[i]
                })
            json_serializer.append({
                'block': 'gallery',
                'content': images
            })
        elif item.name == 'youtube':
            json_serializer.append({
                'block': 'youtube',
                'name': re.findall("\[(.*)\]", item.text)[0],
                'content': re.findall("\((.*)\)", item.text)[0]
            })
    return json_serializer


def get_all_text(html):
    soup = BeautifulSoup(html, features="lxml")
    all_text = ''.join(soup.findAll(text=True))
    return all_text
