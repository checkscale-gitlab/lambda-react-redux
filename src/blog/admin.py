# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin

from .models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'color')
    search_fields = ('name',)


admin.site.register(Category, CategoryAdmin)


class ArticleAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title', 'category', 'status', 'author', 'creation_date', 'order')
    search_fields = ('title', 'description')
    fieldsets = (
        (None, {
            'fields': ('title', 'category', 'description', 'short_description', 'image', 'author', 'tags')
        }),
        ('Настройки публикации', {
            'fields': ('size', 'image_type', 'header_type', 'status', 'publish_date')
        }),
        ('SEO', {
            'fields': ('seo_description', 'keywords')
        }),
    )


admin.site.register(Article, ArticleAdmin)
