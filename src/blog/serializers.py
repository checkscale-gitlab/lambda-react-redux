from rest_framework import serializers
from taggit_serializer.serializers import (TagListSerializerField, TaggitSerializer)

from accounts.models import User
from .models import Article


class WebpImageAuthorSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField(source='thumbnail_webp')


class PngImageAuthorSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField(source='thumbnail_png')


class ImageSerializer(serializers.Serializer):
    original = serializers.ImageField(source='image')
    webp = WebpImageAuthorSerializer(source='*')
    png = PngImageAuthorSerializer(source='*')


class AuthorSerializer(serializers.ModelSerializer):
    """
        Сериалайзер для автора статьи
    """
    name = serializers.ReadOnlyField(source='get_full_name')
    images = ImageSerializer(source='*')

    class Meta:
        model = User
        fields = (
            'name', 'position', 'images'
        )


class WebpImageSerializer(serializers.Serializer):
    compressed = serializers.ImageField(source='compressed_image_webp')
    preload = serializers.ImageField(source='preload_image_webp')
    thumbnail = serializers.ImageField(source='thumbnail_webp')


class PngImageSerializer(serializers.Serializer):
    compressed = serializers.ImageField(source='compressed_image_png')
    preload = serializers.ImageField(source='preload_image_png')
    thumbnail = serializers.ImageField(source='thumbnail_png')


class ImageSerializer(serializers.Serializer):
    original = serializers.ImageField(source='image')
    webp = WebpImageSerializer(source='*')
    png = PngImageSerializer(source='*')


class ArticleSerializer(serializers.ModelSerializer):
    category_name = serializers.ReadOnlyField(source='get_category')
    date_create = serializers.ReadOnlyField(source='get_date_create')
    author = AuthorSerializer(read_only=True, many=False)
    time_read = serializers.ReadOnlyField(source='get_time_read')
    images = ImageSerializer(source='*')

    class Meta:
        model = Article
        fields = (
            'id', 'slug', 'title', 'images', 'category_name', 'size', 'image_type',
            'date_create', 'author', 'time_read'
        )


class ArticleViewSerializer(TaggitSerializer, serializers.ModelSerializer):
    category_name = serializers.ReadOnlyField(source='get_category')
    date_create = serializers.ReadOnlyField(source='get_date_create')
    count_views = serializers.ReadOnlyField(source='get_count_views')
    author = AuthorSerializer(read_only=True, many=False)
    tags = TagListSerializerField()
    images = ImageSerializer(source='*')

    class Meta:
        model = Article
        lookup_field = 'id'
        fields = (
            'title', 'slug', 'images', 'category_name', 'date_create', 'header_type', 'time_read', 'author',
            'short_description', 'count_views', 'content', 'tags', 'seo_description', 'keywords', 'image')
