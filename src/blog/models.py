# -*- coding: utf-8 -*-
import locale

import jsonfield
import readtime
import unidecode
from autoslug import AutoSlugField
from colorfield.fields import ColorField
from django.conf import settings
from django.db import models
from django.template import defaultfilters
from mdeditor.fields import MDTextField
from summa import keywords
from summa.summarizer import summarize
from taggit.managers import TaggableManager

from blog.article_parser import pars_article, get_all_text
from blog.signals import compress_img


class Category(models.Model):
    name = models.CharField(max_length=120, verbose_name='Название')
    slug = models.SlugField()
    color = ColorField(default='#FF0000')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Категории"
        verbose_name = "Категория"


class Article(models.Model):
    """
        Модель Статьи.
        TODO: Добавить аплодисменты как Medium
    """
    # Список выбора размера карточки статьи
    SIZE_CHOICES = (
        ('xxl', "Главная"),
        ('m', "Средняя"),
        ('l', "Большая"),
        ('s', "Маленькая"),
        ('s_wide', "Маленькая широкая"),
    )
    IMAGE_SIZE_CHOICES = (
        ('flat', "Плоское изображение"),
        ('fill', "Заполнение"),
        ('norm', "Нормальное"),
    )
    HEADER_TYPE_CHOICES = (
        ('norm', "С изображением и текстом по левую сторону"),
        ('centered', "С изображением и текстом в середине"),
        ('white', "Без изображения и текстом в середине"),
    )
    STATUS_CHOICES = (
        ('d', 'Черновик'),
        ('p', 'Опубликованый'),
    )

    def get_file_path(self, filename):
        """
            Функция для получения пути для сохранения файла
        :param filename: название файла
        :return: местоположение файла
        """
        return '{0}/{1}/{2}'.format('article', defaultfilters.slugify(unidecode.unidecode(self.title)), filename)

    title = models.CharField(verbose_name='Навзвание', max_length=500)
    slug = AutoSlugField(populate_from='title', default="slug", verbose_name="URL", always_update=True)
    category = models.ForeignKey('Category', default=1, verbose_name="Категория")
    size = models.CharField(choices=SIZE_CHOICES, default='m', verbose_name='Тип', max_length=6)
    image_type = models.CharField(choices=IMAGE_SIZE_CHOICES, default='fill', verbose_name='Тип изображения',
                                  max_length=4)
    header_type = models.CharField(max_length=10, verbose_name="Тип зоголовка при просмотре статьи",
                                   choices=HEADER_TYPE_CHOICES)
    description = MDTextField(verbose_name="Описание")


    short_description = models.TextField(verbose_name="Короткое описание", blank=True, null=True,
                                         help_text="Короткое и внятное описание статьи")

    seo_description = models.TextField(verbose_name="SEO описание", null=True, blank=True)
    keywords = models.TextField(verbose_name="Ключевые слова", null=True, blank=True)

    creation_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Автор", null=True, blank=True)
    tags = TaggableManager(verbose_name="Тэги")
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name="Статус", default='d')
    publish_date = models.DateTimeField(verbose_name="Дата отложенной публикации", null=True, blank=True)
    content = jsonfield.JSONField(null=True, blank=True)
    time_read = models.CharField(max_length=30, null=True, blank=True)

    # Изображения
    image = models.ImageField(verbose_name='Изображение', default='', upload_to=get_file_path, max_length=255)
    compressed_image_webp = models.ImageField(verbose_name="Сжатое изображение WEBP", blank=True, null=True)
    compressed_image_png = models.ImageField(verbose_name="Сжатое изображение PNG", blank=True, null=True)
    preload_image_webp = models.ImageField(verbose_name="Изображение предзагрузки WEBP", blank=True, null=True)
    preload_image_png = models.ImageField(verbose_name="Изображение предзагрузки PNG", blank=True, null=True)
    thumbnail_webp = models.ImageField(verbose_name="Миниатюра WEBP", blank=True, null=True)
    thumbnail_png = models.ImageField(verbose_name="Миниатюра PNG", blank=True, null=True)

    order = models.PositiveIntegerField(default=1, blank=False, null=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"
        ordering = ['-order']


    def get_count_views(self):
        """
            Количество просмотров
        :return: количество просмотров
        """
        view = View.objects.filter(target=self)
        return len(view)

    def get_absolute_url(self):
        """
            Абсолютный url для просмтра в админке
        :return:
        """
        return '/post/{0}'.format(self.slug)

    def get_time_read(self):
        """
            Время чтения статьи
        :return:
        """
        text = ''
        for item in self.get_description():
            if item['block'] == 'text':
                text += item['content']
            elif item['block'] == 'quote':
                text += item['content']['text']
            elif item['block'] == 'code':
                text += item['content']['code']
        return "{0} мин.".format(readtime.of_text(get_all_text(text)).minutes)

    def get_category(self):
        """
            Название и цвет категории
        :return:
        """
        return {
            'name': self.category.name,
            'color': self.category.color
        }

    def get_date_create(self):
        """
            Дата создания статьи, с русской локалью
        :return:
        """
        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        return self.creation_date.strftime("%d %b %H:%M")

    def get_description(self):
        """
            Парсинг статьи для разбиения на блоки
        :return:
        """
        json = pars_article(self.description)
        return json

    def save(self, *args, **kwargs):
        obj = None
        created = False
        try:
            # Пытаемся получить прошлое состояние
            obj = Article.objects.get(id=self.id)
        except Exception as e:
            # Если ничего не получается, то просто создаем объект
            created = True
        # Сохроняем
        instance = super(Article, self).save(*args, **kwargs)
        instance = self
        # Создаем сжатые изображения
        if created or obj.image != instance.image or obj.description != instance.description:
            if created or obj.image != self.image:
                thumbnail_webp, thumbnail_png = compress_img((512, 512), instance.image.path, instance.image.url,
                                                             'thumbnail')
                preload_image_webp, preload_image_png = compress_img((256, 256), instance.image.path,
                                                                     instance.image.url, 'preload', blur=True)
                compressed_image_webp, compressed_image_png = compress_img((1310, 750), instance.image.path,
                                                                           instance.image.url, 'compressed')
                # Сохроняем ссылки на изображения
                # Создаем миниаутюры
                instance.thumbnail_png = thumbnail_png
                instance.thumbnail_webp = thumbnail_webp
                # Создаем размазанные изображения
                instance.preload_image_png = preload_image_png
                instance.preload_image_webp = preload_image_webp
                # Просто сжимаем изображения
                instance.compressed_image_png = compressed_image_png
                instance.compressed_image_webp = compressed_image_webp
            if created or obj.description != instance.description:
                # Получаем информацию о статье
                instance.time_read = self.get_time_read()
                instance.content = self.get_description()
                keyword = keywords.keywords(self.description, ratio=0.1, language='russian')
                instance.seo_description = summarize(self.description, language='russian')
                instance.keywords = '{}'.format(' ,'.join([x for x in keyword.split('\n')]))
            instance.save()
        return instance


class View(models.Model):
    target = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name="Цель", related_name='view_target')
    ip = models.GenericIPAddressField(verbose_name="IP")
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True, verbose_name="Дата просмотра")

    class Meta:
        verbose_name = "Просмотр"
        verbose_name_plural = "Просмотры"
        ordering = ['-date']
