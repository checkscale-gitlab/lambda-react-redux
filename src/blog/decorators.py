from functools import wraps

from blog.models import View, Article


def view_counter(function=None):
    """
       Декоратор для счетчика просмотров студента и компании
    """

    def decorator(func):
        @wraps(func)
        def inner(self, *args, **kwargs):
            student = Article.objects.get(id=self.kwargs['id'])
            new_view = View.objects.create(target=student, ip=self.request.META.get('REMOTE_ADDR', ''))
            new_view.save()
            return func(self, *args, **kwargs)

        return inner

    return decorator
