import logging

from PIL import Image, ImageFilter

logger = logging.getLogger(__name__)


def compress_img(size, img_path, img_url, addition_name, blur=False):
    """

    :param size: turple int
    :param img_path:  string
    :param img_url: string
    :param addition_name: string
    :param blur: bool
    :return:
    """
    out_web = img_path.split('.')[0] + '_' + addition_name + '_' + str(size[0]) + 'x' + str(size[1]) + '.wep'
    out_png = img_path.split('.')[0] + '_' + addition_name + '_' + str(size[0]) + 'x' + str(size[1]) + '.png'

    url_webp = img_url.split('.')[0].split('media/')[1] + '_' + addition_name + '_' + str(size[0]) + 'x' + str(
        size[1]) + '.wep'
    url_png = img_url.split('.')[0].split('media/')[1] + '_' + addition_name + '_' + str(size[0]) + 'x' + str(
        size[1]) + '.png'
    try:
        im = Image.open(img_path)
        im.thumbnail(size)
        if blur:
            im = im.filter(ImageFilter.GaussianBlur(radius=15))
        im.save(out_web, "WEBP", quality=90)
        im.save(out_png, "PNG", quality=90)
        logger.info('create preload_image for thumbnail')
        return (url_webp, url_png)
    except Exception as e:
        logger.error('cannot create compressed', e)
        return (None, None)
