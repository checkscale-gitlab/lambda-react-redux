# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import redirect
from rest_framework import viewsets

from blog.models import Article
from blog.serializers import ArticleSerializer, ArticleViewSerializer


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all().filter(status='p')
    serializer_class = ArticleSerializer
    lookup_field = 'slug'

    def get_object(self):
        self.serializer_class = ArticleViewSerializer
        article = self.queryset.get(slug=self.kwargs['slug'])
        if article.status == 'd':
            if self.request.user.is_staff:
                self.queryset = Article.objects.all()
                return self.queryset.get(slug=self.kwargs['slug'])
            else:
                return redirect('')
        return article
