# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin

from accounts.models import User, SocialNetwork, TeamEvent


# Register your models here.


class BookInline(admin.TabularInline):
    model = SocialNetwork


class UserAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = User
    list_display = (
        '__str__', 'email', 'position', 'order',
    )
    show_change_link = True
    inlines = (
        BookInline,
    )

admin.site.register(User, UserAdmin)


@admin.register(TeamEvent)
class TeamEventAdmun(SortableAdminMixin, admin.ModelAdmin):
    model = TeamEvent
    list_display = (
        'name', 'order',
    )
