# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2019-04-25 12:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_auto_20190425_0317'),
    ]

    operations = [
        migrations.CreateModel(
            name='TeamEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500, verbose_name='Название')),
                ('url', models.URLField(verbose_name='Ссылка')),
                ('image', models.ImageField(default='', upload_to='', verbose_name='Фотография')),
                ('thumbnail_png', models.ImageField(blank=True, default='', null=True, upload_to='', verbose_name='Миниатюра PNG')),
                ('thumbnail_webp', models.ImageField(blank=True, default='', null=True, upload_to='', verbose_name='Миниатюра WEBP')),
            ],
        ),
        migrations.AlterField(
            model_name='socialnetwork',
            name='name',
            field=models.CharField(choices=[('github', 'GitHub'), ('vk', 'Вконтакте'), ('instagram', 'Instagram'), ('twitter', 'Twitter')], max_length=300, verbose_name='Название'),
        ),
    ]
