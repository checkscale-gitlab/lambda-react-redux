from rest_framework import serializers

from accounts.models import User, SocialNetwork, TeamEvent
from lib.utils import validate_email as email_is_valid


class SocialNetworksSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialNetwork
        fields = ('name', 'url')


class WebpImageAuthorSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField(source='thumbnail_webp')
    thumbnail_big = serializers.ImageField(source='thumbnail_512_webp')


class PngImageAuthorSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField(source='thumbnail_png')
    thumbnail_big = serializers.ImageField(source='thumbnail_512_png')


class ImageSerializer(serializers.Serializer):
    original = serializers.ImageField(source='image')
    webp = WebpImageAuthorSerializer(source='*')
    png = PngImageAuthorSerializer(source='*')


class TeamMateSerializer(serializers.ModelSerializer):
    social_networks = SocialNetworksSerializer(many=True, read_only=True)
    images = ImageSerializer(source='*')

    class Meta:
        model = User
        fields = ('email',
                  'first_name',
                  'last_name',
                  'position',
                  'images',
                  'social_networks'
                  )


class WebpImageTeamEventAuthorSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField(source='thumbnail_webp')


class PngImageTeamEventAuthorSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField(source='thumbnail_png')


class ImageTeamEventSerializer(serializers.Serializer):
    original = serializers.ImageField(source='image')
    webp = WebpImageTeamEventAuthorSerializer(source='*')
    png = PngImageTeamEventAuthorSerializer(source='*')


class TeamEventSerializer(serializers.ModelSerializer):
    images = ImageTeamEventSerializer(source='*')

    class Meta:
        model = TeamEvent
        fields = ('name', 'url', 'images')



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name',)


class UserRegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'password')

    def create(self, validated_data):
        """
        Create the object.

        :param validated_data: string
        """
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate_email(self, value):
        """
        Validate if email is valid or there is an user using the email.

        :param value: string
        :return: string
        """

        if not email_is_valid(value):
            raise serializers.ValidationError('Please use a different email address provider.')

        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError('Email already in use, please use a different email address.')

        return value
