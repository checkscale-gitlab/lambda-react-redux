#!/bin/sh

#rm -rf $(find . -name migrations)
#python manage.py makemigrations accounts base blog event --settings=djangoreactredux.settings.prod

echo "MIGRATING DATABASE"
python manage.py migrate --settings=djangoreactredux.settings.prod

echo "COLLECT STATIC"
python manage.py collectstatic --settings=djangoreactredux.settings.prod

echo "RUN UWSGI"
uwsgi --ini /usr/src/app/docker/uwsgi/uwsgi.ini --http :8000
